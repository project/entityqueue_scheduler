<?php
/**
 * @file
 * Entityqueue handler for scheduled queues.
 */

$plugin = array(
  'title' => t('Scheduled queue'),
  'class' => 'EntityQueueSchedulerEntityQueueHandler',
  'weight' => -99,
);
